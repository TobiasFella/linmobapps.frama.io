# Tasks

## Easy tasks, great for first-time contributions:

* Check if apps are available on Arch, Mobian or Flathub.
* If you are running another distribution, feel free to add an extra column for it and start filling it.
* add a new app, e.g.:
  * old Linux Phone apps:
    * Navit
    * FoxtrotGPS
    
  * unoptimized desktop apps that have been run on the PinePhone successfully:  
    * Gnome Photos
    * Gthumb
    * Shotwell // apps from here on where spotted on a screenshot
    * Claws Mail
    * D-Feet
    * Gpredict
    * KeePassXC
    * KiCad
    * Shadowsocks-Qt5
    * [Lyrebird](https://github.com/constcharptr/lyrebird)
    * https://github.com/linuxmint/webapp-manager
    * Verbiste https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7
    * Simple scan https://gitlab.gnome.org/GNOME/simple-scan (newer versions are only fine after "scale-to-fit simple-scan on")
    * Marble (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/27)
    * Orage (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/28)
    * Solar System (https://flathub.org/apps/details/org.sugarlabs.SolarSystem; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/37)
    * AusweisApp 2 (https://flathub.org/apps/details/de.bund.ausweisapp.ausweisapp2; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/51)
    * Nixwriter https://flathub.org/apps/details/com.gitlab.adnan338.Nixwriter; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/69
    * SongRec https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/91
    * Bleachbit https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/101
    * https://github.com/argosopentech/argos-translate
    
  * up and coming smartphone apps (add after testing):
    * PinePhone modem firmware updater UI: https://git.sr.ht/~martijnbraam/pinephone-modemfw (does not work yet, unlikely to be finished due to firmware licensing issues, go to https://github.com/Biktorgj/quectel_eg25_recovery for now)
    * https://invent.kde.org/carlschwan/quickmail
    * https://invent.kde.org/vandenoever/mailmodel
    * https://gitlab.gnome.org/bilelmoussaoui/camera-rs
    * https://gitlab.gnome.org/bilelmoussaoui/paintable-rs
    * https://github.com/OpenMandrivaSoftware/om-phone
    * https://github.com/OpenMandrivaSoftware/om-camera
    * https://gitlab.com/bhdouglass/rockwork
    * https://git.sr.ht/~alva/rocket
    * https://github.com/naxuroqa/venom 
    * https://github.com/MrBn100ful/Hermes-Messenger (Electron)
    * https://github.com/dskleingeld/pods
    * https://github.com/nbdy/pui
    * https://github.com/nahuelwexd/Replay
    * https://github.com/LithApp/Lith 

  * Games to add to the game list:
    * SuperTuxKart (rating: 5)
    * AisleRiot (see https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7)
    * Taquin (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7)
    * Reversi (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/13) 
    * Gnubik (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/26)
    * Foobillardplus (https://github.com/alrusdi/foobillardplus | http://foobillardplus.sourceforge.net/ | https://sourceforge.net/projects/foobillardplus/ ~ https://pkgs.org/search/?q=foobillardplus; https://pkgs.org/search/?q=foobillardplus)
   
## Content maintenance tasks:
* ~~Check and remove further apps that are no longer available (e.g. source code gone) by copying them to archive.csv.~~
* ~~Add games or Apps from "other games.csv" or "other apps.csv".~~
* check links again
* transition "Category" over to Freedesktop [categories](https://specifications.freedesktop.org/menu-spec/latest/apa.html) and [Additional categories](https://specifications.freedesktop.org/menu-spec/latest/apas02.html).
* transition "License" over to [SPDX identifiers](https://spdx.org/licenses/)

## Design tasks:
* Improve mobile design generally.
* Implement a way to display screenshots and or app logos.
* Implement a form with the necessary fields for apps.csv generation for easier email submission.
