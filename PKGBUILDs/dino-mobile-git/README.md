# dino-mobile-git

This is a PKGBUILD to build and install the [feature/handy branch](https://github.com/dino/dino/tree/feature/handy) of [Dino](https://dino.im), a modern XMPP client with great OMEMO support. 
The PKGBUILD was created to work with [Danct12's Arch Linux ARM](http://mobile.danctnix.org), if you build it on stock Arch Linux ARM, change `gtk3-mobile` to `gtk3` in the depends line.
