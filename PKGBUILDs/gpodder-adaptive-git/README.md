# gpodder-adaptive-git

This is a PKGBUILD to build and install tpikonens [hdy branch](https://github.com/tpikonen/gpodder/tree/hdy) of [GPodder](https://gpodder.org), a podfetcher. 
The PKGBUILD was modified based on the AUR pkgbuild gpodder-git in order to work with [Danct12's Arch Linux ARM](http://mobile.danctnix.org), if you build it on stock Arch Linux ARM, change `gtk3-mobile` to `gtk3` in the depends line.
