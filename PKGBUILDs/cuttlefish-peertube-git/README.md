# cuttlefish-peertube-git

This is a PKGBUILD to build and install [Cuttlefish, a GTK4 PeerTube client](https://gitlab.shinice.net/artectrex/Cuttlefish/).
As of January 19th, 2021, the client is still work-in-progress, but it starts already.
