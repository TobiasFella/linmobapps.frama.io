# LINMOBapps

## LINux on MOBile Apps

This project is served at [https://linmobapps.frama.io/](https://linmobapps.frama.io/) and has been forked from the great [https://mglapps.frama.io/](https://mglapps.frama.io) list in order to prepare the creation of a Wiki-based system that lists all the same apps. More Apps and Colums indicating app availablilty across distributions and status have been added.

LINux on MOBile Apps (short: LINMOBapps) is a list of (potential) applications for usage on mobile devices running GNU/Linux, having small(er) screens and touch input (e.g. smartphones, tablets, convertibles).

Files:
* [index.html](index.html): Main page hosted at [https://linmobapps.frama.io/](https://linmobapps.frama.io/)
* [complete.csv](complete.csv): Complete list of apps, to maintain remerge-ability to MGLapps, no longer maintained,
* [apps.csv](apps.csv): Main app list (subset of complete.csv), to be edited directly,
* [games.csv](games.csv): Main games list (subset of complete.csv), to be edited directly.
* [archive.csv](archive.csv): Retired apps (subset of complete.csv).
* [other apps.csv](other apps.csv): Further apps which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the main list

Just like its origin [MGLApps](https://mglapps.frama.io), LINux on MOBile Apps is licensed under CC BY-SA 4.0 International: [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/) . For more licensing information, see [LICENSE](LICENSE)

If you want to help, check [tasks.md](tasks.md)! 

For more information regarding this project, please see [https://linmobapps.frama.io/](https://linmobapps.frama.io/)
